Universidade de Brasília - Faculdade Gama
Matéria: Orientação à Objetos
Professor: Renato Sampaio
Exercício de Programação 3
Alunos: Lucas Soares Souza
Rodrigo Dadamos

*************Definição***************************

Tema:

	Sistema de gerenciamento de acervo (livros, discos, jogos, etc) pessoal on-line.

Objetivo:
	

	Programar um sistema que permita que o usuário, após cadastradado, possa controlar e gerenciar os artigos de seu acervo.

Funcionalidades:

	Ao usuário não cadastrado permite-se:

		*Visualizar as páginas: inicial, cadastre-se, sobre, serviços e contatos.

	Ao usuário cadastrado permite-se:

		*Possuir um perfil particular e acessível somente por ele.
		*Cadastrar, editar e destruir, em seu acervo, itens padrão como: livros, albuns e jogos.
		*Cadastrar, editar e destruir empréstimos de itens avulsos do seu acervo.
		*Listar cada um destes cadastros anteriores.
		*Cancelar a sua conta e junto com ela apagar todos seus dados do sistema.

	Ao usuário adminstrador permite-se:

		*Apagar toda e qualquer conta de úsuario do sistema.


*************Programas necessários*****************


		Ruby 2.1.1p76 ou versão similar
		Rails 4.2.6 ou versão similar
		ImageMagick*

		*Para instalar o ImageMagick:

		No MAC OS X execute no terminal: 

			brew install imagemagick

		No Ubuntu execute no terminal: 

			sudo apt-get install imagemagick


*************Configurando o Sistema*****************


		Abra o console e execute o comando: which convert
		Caso o ImageMagick tenha sido instalado corretamente ele retornará um caminho como "/usr/bin/convert"
		Copie o endereço que vem antes da palavra 'convert', ou seja "usr/bin/"
		Siga para a pasta EP3/config/environments/ e abra o arquivo 'development.rb' e coloque a seguinte linha:
		'Paperclip.options[:command_path] = "caminho"' sendo 'caminho' o endereço que which convert retornou.

		Agora, no console, siga para a pasta EP3 e execute as linhas:
		bundle install
		rake db:migrate
		rails s

		Caso tudo ocorra corretamente o servidor estará no ar.
		Abra o navegador e acesse: localhost:3000
		Para encerrar o servidor no terminal aperte as teclas "ctrl + c".


*************Utilizando o Site***********************

		*Efetue um cadastro para que possa ter acesso ao conteudo interno do site. Caso você não faça um cadastro será permitido acessar, somente, as abas 'Sobre', 'Serviço', 'Contato', Login e Cadastro.

		*Após cadastrado o usuario é redirecionado para a sua home onde pode cadastrar seus itens em seu acervo pessoal.

		*Para cadastrar um item basta que o usuario clicle em um dos botões "Novo 'x'" e complete todos os campos do cadastro do item e confirme no final.

		*Os itens cadastrados serão listados em sua home. Cada um deles pode ser: editado, mostrado e deletado.	Ao deletar um item este não poderá ser recuperado.
		
		*O cadastro de empréstimo serve para que o usuario gerencie os empréstimos que fez de seu acervo.
		Esta ferramenta funciona separadamente dos outros cadastros.

		*Quando um item estiver emprestado o campo representando tal ação na home do usuário será marcado de vermelho. Caso o ite não esteja emprestado o campo será marcado de verde.

		*O Usuario pode ainda editar seu perfil e sua foto. Sendo que em todos os casos ele deve fornecer a senha de sua conta para que o cadastro seja realmente atualizado. E pode deletar sua propria conta.

		*A aba 'Logout' encerra a sessão do usuario.

		**Obs:O primeiro campo de cadastro de itens para os usuários possui o numero do id dele no banco de dados.Este campo não pode ser editado pois este numero é sua referência no banco de dados do sistema. Por tal motivo este campo está programado para "somente leitura".

*************Adicionando um administrador*************

		Para adicionar um administrador abra o terminal no diretório .../EP3 e, com o console desligado, execute:

		$ rails console
		#O console do rails será aberto. Siga o seguinte processo no console:
		> senha = "sua senha desejada"
		> adm = Admin.new(email:"seu@email.com", password:senha, password_confirmation:senha)
		> adm.save
		> exit

		*A senha deve conter no mínimo 8 caracteres.

		Caso o comando adm.save retorne "true" o administrador foi adicionado com sucesso.
		Para acessar a pagina do administrador no navegador acesse:
		http://localhost:3000/admins/sign_in
		Efetue o login com o email e a senha de administrador e será redirecionado para a home da administração.
		
		*O administrador pode deletar qualquer conta de usuario comum encontrada no sistema. Ao apagar a conta de um usuario User ficará impossibilitado de acessar o site com a conta antiga e perderá todos os dados cadastrados nela.
		
		*Quando o administrador apaga a conta de um usuario também apaga os dados e os registros do usuario do sistema.

		*A pagina da adminstração só pode ser acessada por usuarios adm.
		*O administrador não tem acesso direto aos itens listados no banco de dados pelos usuarios.

		*O link "desconectar administrador" encerra a sessão do administrador.


**************Descrição das Classes Model*************

	

	=>Model user.rb

	Resposável pelo CRUD do Usuário.

	Atributos:

		string: name
		string: password
		string: email
		string: image

	Métodos:

		User.new(email, password, password_confirmation, name, image) - Retorna um novo usuário.
		User.Destroy(@user) -  Destroi um usuário cadastrado.
		update - edita o usuário

	

	=>Model admin.rb

	Atributos:

		string: email
		string: password

	Métodos:

		Admin.new(email, password, password_confirmation) - Retorna um novo administrador.

	

	=>Model album.rb

	Atributos:

		string: iduser
		string: name
		string: band
		string: description
		boolan: borrow

	Métodos:

		index - a página recebe acesso a todos os albuns do banco de dados.
		show - motra um album específico.
		new - cria um novo album.
		create - cria um novo album com parâmetros.
		update - edita um album já criado.
		destroy - deleta o album.


	=>Model books.rb

	Atributos:

		string: iduser
		string: titlee
		string: author
		string: description
		boolean: borrow

	Métodos:

		index - a página recebe acesso a todos os livros do banco de dados.
		show - motra um livro específico.
		new - cria um novo livro.
		create - cria um novo livro com parâmetros.
		update - edita um livro já criado.
		destroy - deleta o livro.		


	=> Model borrow.rb

	Atributos

		string: iduser
		string: idtitle
		string: renter
		string: initialdade
		string: finaldate
		string: itemtype

	Métodos:

		index - a página recebe acesso a todos os empréstimos do banco de dados.
		show - motra um empréstimo específico.
		new - cria um novo empréstimo.
		create - cria um novo empréstimo com parâmetros.
		update - edita um empréstimo já criado.
		destroy - deleta o empréstimo.



	=> Model games.rb

		string: iduser
		string: title
		string: developer
		string: description
		boolean: borrow

	Métodos:

		index - a página recebe acesso a todos os ejogos do banco de dados.
		show - motra um jogo específico.
		new - cria um novo jogo.
		create - cria um novo jogo com parâmetros.
		update - edita um jogo já criado.
		destroy - deleta o jogo.

** Obs1: O método para destruir os usuarios criados pela gem devise só permitia que um úsuario logado deletasse sua própria conta. Então, para implementação de um adminstrador, foi criada uma controller chamada "users_controller" com o método 'destroy'. Este método é utilizado na controller "adm_page_controller" justamente para que o adminstrador possa deletar as contas dos usuarios.

** Obs2: Na aplication_controller.rb foi criado um método chamado "after_sign_in_path_for" para que o sistema indentifique o usuario logado e o redirecione para sua pagina desejada.

**************************************************************************