json.array!(@borrows) do |borrow|
  json.extract! borrow, :id, :iduser, :idtitle, :renter
  json.url borrow_url(borrow, format: :json)
end
