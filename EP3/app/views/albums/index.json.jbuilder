json.array!(@albums) do |album|
  json.extract! album, :id, :iduser, :band, :description, :borrow
  json.url album_url(album, format: :json)
end
