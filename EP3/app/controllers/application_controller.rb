class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters 
  devise_parameter_sanitizer.for(:sign_up) << :name
  devise_parameter_sanitizer.for(:account_update) << :name
  devise_parameter_sanitizer.for(:sign_up) << :image  
  devise_parameter_sanitizer.for(:account_update) << :image
  end
  
  def after_sign_in_path_for(resource)
  	if (user_signed_in? == true)
  		home_user_index_path
  	elsif(admin_signed_in? == true)
       adm_page_index_path 
    else
      super
    end
  end

end
