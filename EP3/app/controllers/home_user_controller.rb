class HomeUserController < ApplicationController
	before_action :authenticate_user!
  def index
    @books = Book.all
    @games = Game.all
    @albums = Album.all
    @borrows = Borrow.all
  end
end
