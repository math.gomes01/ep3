class Borrow < ActiveRecord::Base
	validates_presence_of :idtitle, :renter
	validates :initialdate, presence: true
	validates :finaldate, presence: true
	validates :itemtype, presence: true
end
