class CreateBorrows < ActiveRecord::Migration
  def change
    create_table :borrows do |t|
      t.string :iduser
      t.string :idtitle
      t.string :renter

      t.timestamps null: false
    end
  end
end
